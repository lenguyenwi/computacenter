﻿using KfzKonfiguration.Context;
using KfzKonfiguration.EntityFramework;
using KfzKonfiguration.Helpers;
using KfzKonfiguration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace KfzKonfiguration.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Configuration
        [HttpGet]
        [ActionName("GetAutoById")]
        public ActionResult GetAutoById(string id)
        {

            Auto auto = null;
            using (var entities = new ConfigKfzDBEntities())
            {

                auto = entities.Autos.Include("Getriebes")
                    .Include("Farbes").Include("Felgenes")
                    .Include("Motoleistunges")
                    .Include(" Sonderausstattungenes")
                    .FirstOrDefault(au => au.AutoId.Equals(id, StringComparison.InvariantCultureIgnoreCase));
                if (auto == null)
                {
                    var message = "Not found auto with Id " + id;
                    Log4NetHelper.LogInfo(message);
                    throw new KeyNotFoundException(message);
                }
            }
            return View(auto);
        }

        [HttpGet]
        [ActionName("GetAutos")]
        public JsonResult GetAutos()
        {
            List<Object> listAuto = new List<Object>();

            using (var entities = new ConfigKfzDBEntities())
            {
                listAuto = (from e in entities.Autos
                            select new
                            {
                                Seqid = e.Seqid,
                                AutoName = e.AutoName
                            }).ToList<Object>();
            }

            return Json(listAuto, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetElementsByAuto")]
        public JsonResult GetElementsByAuto(int autonumber)//different Autoid
        {
            Dictionary<String, List<Object>> autoElements = new Dictionary<String, List<Object>>();

            List<Object> listFarbe = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listFarbe = (from f in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Farbes
                             select new { Seqid = f.Seqid, FarbeName = f.FarbeName }).ToList<Object>();
            }
            return Json(listFarbe, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetFarbesByAuto")]
        public JsonResult GetFarbesByAuto(int autonumber)//different Autoid
        {

            Log4NetHelper.LogInfo("choosed Farbe");
            List<Object> listFarbe = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listFarbe = (from f in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Farbes
                             select new { Seqid = f.Seqid, FarbeName = f.FarbeName, FarbePrice = f.Price }).ToList<Object>();
            }
            return Json(listFarbe, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetGetriebesByAuto")]
        public JsonResult GetGetriebesByAuto(int autonumber)//different Autoid
        {
            List<Object> listGetriebes = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listGetriebes = (from g in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Getriebes
                                 select new { Seqid = g.Seqid, GetriebeName = g.GetriebeName, GetriebePrice = g.Price }).ToList<Object>();
            }
            return Json(listGetriebes, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetMotoleistungesByAuto")]
        public JsonResult GetMotoleistungesByAuto(int autonumber)//different Autoid
        {
            List<Object> listMotoleistungs = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listMotoleistungs = (from m in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Motoleistunges
                                     select new { Seqid = m.Seqid, MotoleistungName = m.MotoleistungName, MotoleistungPrice = m.Price }).ToList<Object>();
            }
            return Json(listMotoleistungs, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetFelgenesByAuto")]
        public JsonResult GetFelgenesByAuto(int autonumber)//different Autoid
        {
            List<Object> listFelgenes = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listFelgenes = (from f in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Felgenes
                                select new { Seqid = f.Seqid, FelgenName = f.FelgenName, FelgenPrice = f.Price }).ToList<Object>();
            }
            return Json(listFelgenes, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionName("GetSonderausstattungenesByAuto")]
        public JsonResult GetSonderausstattungenesByAuto(int autonumber)//different Autoid
        {
            List<Object> listSonderausstattungenes = new List<Object>();
            using (var entities = new ConfigKfzDBEntities())
            {
                listSonderausstattungenes = (from s in entities.Autos.FirstOrDefault(a => a.Seqid == autonumber).Sonderausstattungenes
                                             select new { Seqid = s.Seqid, SonderausstattungenName = s.SonderausstattungenName, SonderausstattungenPrice = s.Price }).ToList<Object>();

            }
            return Json(listSonderausstattungenes, JsonRequestBehavior.AllowGet);

        }

        //[Authorize]
        [HttpGet]
        [ActionName("Service")]
        public ActionResult GetService()
        {
            //IList <AutoResponse> auto = null;
            //using (var entities = new ConfigKfzDBEntities())
            //{
            //    auto = entities.Autos.Select(x => new AutoResponse() {
            //        Seqid = x.Seqid,
            //        AutoCode = x.AutoCode,
            //        AutoId = x.AutoId,
            //        AutoName = x.AutoName
            //    }).ToList();
            //}
            return View();
        }

        private void CalculateTotalFee(AutoTicket autoTicket)
        {
            double total = 0.0;
            if (autoTicket.Farbes != null)
            {
                total += autoTicket.Farbes.Price;
            }
            if (autoTicket.Felgenes != null)
            {
                total += autoTicket.Felgenes.Price;
            }
            if (autoTicket.Getriebes != null)
            {
                total += autoTicket.Getriebes.Price;
            }
            if (autoTicket.Motoleistunges != null)
            {
                total += autoTicket.Motoleistunges.Price;
            }
            if (autoTicket.Sonderausstattungenes != null)
            {
                total += autoTicket.Sonderausstattungenes.Sum(x => x.Price);
            }
            autoTicket.TotalPrice = total;
        }
        [HttpGet]
        [ActionName("Ticket")]
        public ActionResult GetTicket(string ticketId)
        {
            AutoTicket autoTicket = null;
            try
            {
                using (var entities = new ConfigKfzDBEntities())
                {
                    var order = entities.Orders.FirstOrDefault(t => t.OrderTicket == ticketId);
                    if (order != null)
                    {
                        var auto = (Auto)Newtonsoft.Json.JsonConvert.DeserializeObject(order.DetailOrder, typeof(Auto));
                        autoTicket = new AutoTicket()
                        {
                            TicketNumber = order.OrderTicket,
                            AutoCode = auto.AutoCode,
                            AutoId = auto.AutoId,
                            AutoName = auto.AutoName,
                            Farbes = auto.Farbes.FirstOrDefault(),
                            Felgenes = auto.Felgenes.FirstOrDefault(),
                            Getriebes = auto.Getriebes.FirstOrDefault(),
                            Motoleistunges = auto.Motoleistunges.FirstOrDefault(),
                            Sonderausstattungenes = auto.Sonderausstattungenes != null ? auto.Sonderausstattungenes.ToList() : null,
                            CreatedDate = order.CreateDate
                        };
                        CalculateTotalFee(autoTicket);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.LogError("Get ticket - " + ticketId + " : " +  ex.Message, ex);
            }
            return View(autoTicket);
        }


        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
        [HttpPost]
        [ActionName("CreateTicketOrder")]
        public JsonResult CreateTicketOrder(OrderTicketRequest request)
        {

            var urlReviewTicket = string.Empty;
            try
            {
                Order orderTicket = null;
                using (var kfzContext = new ConfigKfzDBEntities())
                {
                   
                    var auto = kfzContext.Autos.First(au => au.Seqid == request.AutoNumber);
                    auto.Farbes = auto.Farbes.Where(f => f.Seqid == request.FaberId).ToList();
                    auto.Getriebes = auto.Getriebes.Where(g => g.Seqid == request.GetriebeId).ToList();
                    auto.Motoleistunges = auto.Motoleistunges.Where(m => m.Seqid == request.MotoleistungId).ToList();
                    auto.Felgenes = auto.Felgenes.Where(m => m.Seqid == request.FelgeneId).ToList();
                    auto.Sonderausstattungenes = auto.Sonderausstattungenes.Where(s => request.SonderausstattungenesIds.Any(x => x == s.Seqid)).ToList();
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(auto);
                    // Base on userId, we can get email information.So, I will hard code email value.
                    // Same way for UserId
                    orderTicket = new Order()
                    {
                        CreateDate = DateTime.Now,
                        DetailOrder = jsonString,
                        Email = "tnguyen@icloud.com",
                        IsSentEmail = true,
                        Note = string.Empty,
                        OrderTicket = GetTimestamp(DateTime.Now),
                        UserId = "SampleAccountId"
                    };
                }
                
                using (var kfzContext = new ConfigKfzDBEntities())
                {
                    kfzContext.Orders.Add(orderTicket);
                    kfzContext.SaveChanges();
                    urlReviewTicket = this.Url.Action("Ticket", "Configuration", new { ticketId = orderTicket.OrderTicket }, this.Request.Url.Scheme);
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.LogError("Create ticket order: " + ex.Message, ex);

                return Json(new ResponseData()
                {
                    Result = "error",
                    Message = ex.Message
                });
            }

            return Json(new ResponseData()
            {
                Result = "success",
                Message = urlReviewTicket
            });
        }

    }

    public class ResponseData {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}