namespace KfzKonfiguration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Autos",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        AutoId = c.String(nullable: false, maxLength: 100),
                        AutoName = c.String(nullable: false, maxLength: 100),
                        AutoCode = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Farbes",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        FarbeName = c.String(nullable: false, maxLength: 100),
                        FarbeCode = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        Price = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Felgenes",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        FelgenName = c.String(nullable: false, maxLength: 100),
                        FelgenCode = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        Price = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Getriebes",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        GetriebeName = c.String(nullable: false, maxLength: 100),
                        GetriebeCode = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        Price = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Motoleistunges",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        MotoleistungName = c.String(nullable: false, maxLength: 100),
                        MotoleistungCode = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        Price = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Sonderausstattungenes",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        SonderausstattungenName = c.String(nullable: false, maxLength: 100),
                        SonderausstattungenCode = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 100),
                        Price = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Seqid = c.Int(nullable: false, identity: true),
                        OrderTicket = c.String(nullable: false, maxLength: 100),
                        UserId = c.String(nullable: false, maxLength: 100),
                        Note = c.String(maxLength: 100),
                        Email = c.String(maxLength: 100),
                        DetailOrder = c.String(nullable: false, maxLength: 2000),
                        IsSentEmail = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Seqid);
            
            CreateTable(
                "dbo.FarbeAutoes",
                c => new
                    {
                        Farbe_Seqid = c.Int(nullable: false),
                        Auto_Seqid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Farbe_Seqid, t.Auto_Seqid })
                .ForeignKey("dbo.Farbes", t => t.Farbe_Seqid, cascadeDelete: true)
                .ForeignKey("dbo.Autos", t => t.Auto_Seqid, cascadeDelete: true)
                .Index(t => t.Farbe_Seqid)
                .Index(t => t.Auto_Seqid);
            
            CreateTable(
                "dbo.FelgenAutoes",
                c => new
                    {
                        Felgen_Seqid = c.Int(nullable: false),
                        Auto_Seqid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Felgen_Seqid, t.Auto_Seqid })
                .ForeignKey("dbo.Felgenes", t => t.Felgen_Seqid, cascadeDelete: true)
                .ForeignKey("dbo.Autos", t => t.Auto_Seqid, cascadeDelete: true)
                .Index(t => t.Felgen_Seqid)
                .Index(t => t.Auto_Seqid);
            
            CreateTable(
                "dbo.GetriebeAutoes",
                c => new
                    {
                        Getriebe_Seqid = c.Int(nullable: false),
                        Auto_Seqid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Getriebe_Seqid, t.Auto_Seqid })
                .ForeignKey("dbo.Getriebes", t => t.Getriebe_Seqid, cascadeDelete: true)
                .ForeignKey("dbo.Autos", t => t.Auto_Seqid, cascadeDelete: true)
                .Index(t => t.Getriebe_Seqid)
                .Index(t => t.Auto_Seqid);
            
            CreateTable(
                "dbo.MotoleistungAutoes",
                c => new
                    {
                        Motoleistung_Seqid = c.Int(nullable: false),
                        Auto_Seqid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Motoleistung_Seqid, t.Auto_Seqid })
                .ForeignKey("dbo.Motoleistunges", t => t.Motoleistung_Seqid, cascadeDelete: true)
                .ForeignKey("dbo.Autos", t => t.Auto_Seqid, cascadeDelete: true)
                .Index(t => t.Motoleistung_Seqid)
                .Index(t => t.Auto_Seqid);
            
            CreateTable(
                "dbo.SonderausstattungenAutoes",
                c => new
                    {
                        Sonderausstattungen_Seqid = c.Int(nullable: false),
                        Auto_Seqid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sonderausstattungen_Seqid, t.Auto_Seqid })
                .ForeignKey("dbo.Sonderausstattungenes", t => t.Sonderausstattungen_Seqid, cascadeDelete: true)
                .ForeignKey("dbo.Autos", t => t.Auto_Seqid, cascadeDelete: true)
                .Index(t => t.Sonderausstattungen_Seqid)
                .Index(t => t.Auto_Seqid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SonderausstattungenAutoes", "Auto_Seqid", "dbo.Autos");
            DropForeignKey("dbo.SonderausstattungenAutoes", "Sonderausstattungen_Seqid", "dbo.Sonderausstattungenes");
            DropForeignKey("dbo.MotoleistungAutoes", "Auto_Seqid", "dbo.Autos");
            DropForeignKey("dbo.MotoleistungAutoes", "Motoleistung_Seqid", "dbo.Motoleistunges");
            DropForeignKey("dbo.GetriebeAutoes", "Auto_Seqid", "dbo.Autos");
            DropForeignKey("dbo.GetriebeAutoes", "Getriebe_Seqid", "dbo.Getriebes");
            DropForeignKey("dbo.FelgenAutoes", "Auto_Seqid", "dbo.Autos");
            DropForeignKey("dbo.FelgenAutoes", "Felgen_Seqid", "dbo.Felgenes");
            DropForeignKey("dbo.FarbeAutoes", "Auto_Seqid", "dbo.Autos");
            DropForeignKey("dbo.FarbeAutoes", "Farbe_Seqid", "dbo.Farbes");
            DropIndex("dbo.SonderausstattungenAutoes", new[] { "Auto_Seqid" });
            DropIndex("dbo.SonderausstattungenAutoes", new[] { "Sonderausstattungen_Seqid" });
            DropIndex("dbo.MotoleistungAutoes", new[] { "Auto_Seqid" });
            DropIndex("dbo.MotoleistungAutoes", new[] { "Motoleistung_Seqid" });
            DropIndex("dbo.GetriebeAutoes", new[] { "Auto_Seqid" });
            DropIndex("dbo.GetriebeAutoes", new[] { "Getriebe_Seqid" });
            DropIndex("dbo.FelgenAutoes", new[] { "Auto_Seqid" });
            DropIndex("dbo.FelgenAutoes", new[] { "Felgen_Seqid" });
            DropIndex("dbo.FarbeAutoes", new[] { "Auto_Seqid" });
            DropIndex("dbo.FarbeAutoes", new[] { "Farbe_Seqid" });
            DropTable("dbo.SonderausstattungenAutoes");
            DropTable("dbo.MotoleistungAutoes");
            DropTable("dbo.GetriebeAutoes");
            DropTable("dbo.FelgenAutoes");
            DropTable("dbo.FarbeAutoes");
            DropTable("dbo.Order");
            DropTable("dbo.Sonderausstattungenes");
            DropTable("dbo.Motoleistunges");
            DropTable("dbo.Getriebes");
            DropTable("dbo.Felgenes");
            DropTable("dbo.Farbes");
            DropTable("dbo.Autos");
        }
    }
}
