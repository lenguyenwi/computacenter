namespace KfzKonfiguration.Migrations
{
    using KfzKonfiguration.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KfzKonfiguration.Context.ConfigKfzDBEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(KfzKonfiguration.Context.ConfigKfzDBEntities context)
        {
            // Deletes all data, from all tables, except for __MigrationHistory
            //context.Database.ExecuteSqlCommand("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'");
            //context.Database.ExecuteSqlCommand("sp_MSForEachTable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)) DELETE FROM ?'");
            //context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'");

            // proceed with the seed here

            var auto318i = new Auto()
            {
                AutoId = "1",
                AutoCode = "318i",
                AutoName = "BMW 318i Limousine"
            };

            var auto320i = new Auto()
            {
                AutoId = "2",
                AutoCode = "320i",
                AutoName = "BMW 320i Limousine"
            };
            #region Loai May
            //for 318i
            var engines = new List<Tuple<string, double, string>>();
            engines.Add(new Tuple<string, double, string>("Manuell", 1000, "#GETRIEBE KPL .# 63533000000"));
            engines.Add(new Tuple<string, double, string>("Automatic Getriebe Steptronic", 3000, "#GETRIEBE KPL .# 6000000"));
            foreach (var value in engines)
            {
                var temp = new Getriebe()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    GetriebeCode = value.Item3,
                    GetriebeName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };

                auto318i.Getriebes.Add(temp);
                auto320i.Getriebes.Add(temp);
            }
            //for 320i
            //Dictionary<string, double> engines320i = new Dictionary<string, double>();
            //engines320i.Add("Manuell", 1000);
            //engines320i.Add("Automatic Getriebe Steptronic", 3000);
            //engines320i.Add("Sport-Automatic Getriebe Steptronic", 5000);
            //foreach (var value in engines320i)
            //{
            //    var temp = new Getriebe()
            //    {
            //        CreateDate = DateTime.Now,
            //        Description = string.Empty,
            //        GetriebeCode = Guid.NewGuid().ToString(),
            //        GetriebeName = value.Key,
            //        ModifiedDate = DateTime.Now,
            //        Price = value.Value,

            //    };
            //    //auto318i.Getriebes.Add(temp);
            //    auto320i.Getriebes.Add(temp);
            //}

            #endregion

            #region Colors
            var colors = new List<Tuple<string, double, string>>();
            colors.Add( new Tuple<string, double, string>("Rot", 100, "#FF0000"));
            colors.Add(new Tuple<string, double, string>("Blau", 200, "#0000FF"));
            colors.Add(new Tuple<string, double, string>("Schwarz", 10, "#FFA500"));
            foreach (var value in colors)
            {
                var temp = new Farbe()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    FarbeCode = value.Item3,
                    FarbeName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };
                auto318i.Farbes.Add(temp);
            }

            colors = new List<Tuple<string, double, string>>();
            colors.Add( new Tuple<string, double, string>("Rot", 100, "#A52A2A"));
            colors.Add(new Tuple<string, double, string>("Blau", 200, "#808000"));
            colors.Add(new Tuple<string, double, string>("Schwarz", 10, "#FF00FF"));
            foreach (var value in colors)
            {
                var temp = new Farbe()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    FarbeCode = value.Item3,
                    FarbeName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };
                auto320i.Farbes.Add(temp);
            }
            #endregion

            #region Maluc
            var Maluc = new List<Tuple<string, double, string>>();
            Maluc.Add(new Tuple<string, double, string>("118ps", 10000, "NO:14233 -57A-001"));
            Maluc.Add(new Tuple<string, double, string>("136ps", 20000, "NO:2001C5W4 H7000"));
            foreach (var value in Maluc)
            {
                var temp = new Motoleistung()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    MotoleistungCode = value.Item3,
                    MotoleistungName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };
                auto318i.Motoleistunges.Add(temp);
            }

            Maluc = new List<Tuple<string, double, string>>();
            Maluc.Add(new Tuple<string, double, string>("150ps", 100000, "NO:2001C5W4 H700"));
            Maluc.Add(new Tuple<string, double, string>("184ps", 200000, "NO:2001D-SES-7700"));
            foreach (var value in Maluc)
            {
                var temp = new Motoleistung()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    MotoleistungCode = value.Item3,
                    MotoleistungName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };
                auto320i.Motoleistunges.Add(temp);
            }
            #endregion

            #region Mamxe
            var Mamxe = new List<Tuple<string, double, string>>();
            Mamxe.Add(new Tuple<string, double, string>("Stahlrad", 1580, "#63533013000"));
            Mamxe.Add(new Tuple<string, double, string>("Leichtmetallrad", 2652, "#63533012000"));
            foreach (var value in Mamxe)
            {
                var temp = new Felgen()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    FelgenCode = value.Item3,
                    FelgenName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2,

                };
                auto318i.Felgenes.Add(temp);
                auto320i.Felgenes.Add(temp);
            }

            #endregion;

            #region components
            var components = new List<Tuple<string, double, string>>();
            components.Add(new Tuple<string, double, string>("Klimaanlage", 5000, "NO.E722892"));
            components.Add(new Tuple<string, double, string>("Soundsystem", 7542, "350A33-0200-06"));
            components.Add(new Tuple<string, double, string>("Fahrsicherheitssysteme", 8542, "NO.E71111"));
            components.Add(new Tuple<string, double, string>("Park Distance Control (PDC)", 9542, "NO.E5562"));
            components.Add(new Tuple<string, double, string>("Geschwindigkeitsregelung mit Bremsfunktion", 4542, "NO.E73422"));
            components.Add(new Tuple<string, double, string>("Ablagenpaket", 4782, "NO.E77892"));
            foreach (var value in components)
            {
                var temp = new Sonderausstattungen()
                {
                    CreateDate = DateTime.Now,
                    Description = string.Empty,
                    SonderausstattungenCode = value.Item3,
                    SonderausstattungenName = value.Item1,
                    ModifiedDate = DateTime.Now,
                    Price = value.Item2
                };
                auto318i.Sonderausstattungenes.Add(temp);
                auto320i.Sonderausstattungenes.Add(temp);
            }

            #endregion
            context.Autos.Add(auto320i);
            context.Autos.Add(auto318i);
            context.SaveChanges();
        }
    }
}
