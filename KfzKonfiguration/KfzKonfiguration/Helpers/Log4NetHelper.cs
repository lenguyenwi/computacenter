﻿using System;
using log4net;
using log4net.Config;

namespace KfzKonfiguration.Helpers
{
    public static class Log4NetHelper
    {
        private static readonly ILog logger = LogManager.GetLogger("EventLogFileAppender");


        static Log4NetHelper()
        {
            XmlConfigurator.Configure();
        }


        public static void LogInfo(string message)
        {
            logger.Info(message);
        }

        public static void LogError(string message, Exception ex)
        {
            logger.Error(message, ex);
        }

        public static void LogWarning(string message, Exception ex)
        {
            logger.Warn(message, ex);
        }
    }
}