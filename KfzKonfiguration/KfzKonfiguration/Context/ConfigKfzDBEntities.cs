﻿using KfzKonfiguration.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.Context
{
    public class ConfigKfzDBEntities : DbContext
    {
        public ConfigKfzDBEntities() : base("name=KonfigurationKfzConnectionString")
        {
            
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }


        public DbSet<Auto> Autos { get; set; }
        public DbSet<Getriebe> Getriebes { get; set; }
        public DbSet<Farbe> Farbes { get; set; }
        public DbSet<Felgen> Felgenes { get; set; }
        public DbSet<Motoleistung> Motoleistunges { get; set; }
        public DbSet<Sonderausstattungen> Sonderausstattungenes { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}