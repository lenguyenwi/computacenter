﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.EntityFramework
{
    [Table("Autos")]
    public class Auto : BaseEntity
    {
        public Auto()
        {
            Getriebes = new HashSet<Getriebe>();
            Farbes = new HashSet<Farbe>();
            Felgenes = new HashSet<Felgen>();
            Motoleistunges = new HashSet<Motoleistung>();
            Sonderausstattungenes = new HashSet<Sonderausstattungen>();
        }

        [Required]
        [StringLength(100)]
        public string AutoId { get; set; }

        [Required]
        [StringLength(100)]
        public string AutoName { get; set; }

        [Required]
        [StringLength(100)]
        public string AutoCode { get; set; }
        public virtual ICollection<Getriebe> Getriebes { get; set; }
        public virtual ICollection<Farbe> Farbes { get; set; }
        public virtual ICollection<Felgen> Felgenes { get; set; }
        public virtual ICollection<Motoleistung> Motoleistunges { get; set; }
        public virtual ICollection<Sonderausstattungen> Sonderausstattungenes { get; set; }
    }
}