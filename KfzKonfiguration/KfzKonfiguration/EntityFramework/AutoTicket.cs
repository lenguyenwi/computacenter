﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.EntityFramework
{
    public class AutoTicket
    {
        public string TicketNumber { get; set; }
        public string AutoId { get; set; }
        public string AutoName { get; set; }
        public string AutoCode { get; set; }
        public Getriebe Getriebes { get; set; }
        public Farbe Farbes { get; set; }
        public Felgen Felgenes { get; set; }
        public Motoleistung Motoleistunges { get; set; }
        public List<Sonderausstattungen> Sonderausstattungenes { get; set; }
        public DateTime? CreatedDate { get; set; }
        public double TotalPrice { get; set; }
    }
}