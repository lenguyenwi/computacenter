﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.EntityFramework
{
    [Table("Getriebes")]
    public class Getriebe : BaseEntity
    {
        public Getriebe()
        {
            Autos = new HashSet<Auto>();
        }

        [Required]
        [StringLength(100)]
        public string GetriebeName { get; set; }

        [Required]
        [StringLength(50)]
        public string GetriebeCode { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public double Price { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        [JsonIgnore]
        public virtual ICollection<Auto> Autos { get; set; }
    }
}