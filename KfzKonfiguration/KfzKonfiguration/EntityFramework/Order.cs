﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace KfzKonfiguration.EntityFramework
{
    [Table("Order")]
    public class Order : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string OrderTicket { get; set; }
        [Required]
        [StringLength(100)]
        public string UserId { get; set; }

        [StringLength(100)]
        public string Note { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(2000)]
        public string DetailOrder { get; set; }
        public bool IsSentEmail { get; set; }
        public DateTime CreateDate { get; set; }


    }
}