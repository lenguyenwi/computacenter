﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.Models
{
    public class OrderTicketRequest
    {
        public int AutoNumber { get; set; }
        public int FaberId { get; set; }
        public int GetriebeId { get; set; }
        public int MotoleistungId { get; set; }
        public int FelgeneId { get; set; }
        public int[] SonderausstattungenesIds { get; set; }
    }
}