﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KfzKonfiguration.Models
{
    public class AutoResponse
    {
        public int Seqid { get; set; }
        public string AutoId { get; set; }
        public string AutoName { get; set; }
        public string AutoCode { get; set; }
    }
}